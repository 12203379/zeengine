# Équipe ZEEngine

L'équipe est composée de 5 membres:

- **Mohamed RAHMANI** : Développeur Full Stack
- **Chemsedine BENHADDOU** : Sécurité des données
- **Rayan RAMASSAMY** : Développeur Front-end
- **Akirthan NAGULENDRAN** : Développeur Front-end
- **Abdel-Ghani GHAMALLAH** : Développeur Front-end

## Site intranet pour l'entreprise Perform Vision

Dans le cadre des activités de la SAS Perform Vision, présidée par Slim ELLOUZE, cette dernière est amenée à proposer à ses clients de la prestation en régie. 
Pour être plus précis, chaque client est subdivisé en composantes, pour lequel il peut y avoir plusieurs interlocuteurs (et un interlocuteur peut “gérer” plusieurs composantes) 

Actuellement, afin de faciliter la facturation de ces prestations, chaque prestataire émet un bon de livraison (BDL) par composantes et par mois afin d’y consigner le nombre d’heures effectuées (et éventuellement des commentaires) au cours de chacune des journées (du mois) au sein de la composante. Chaque BDL doit ensuite être visé par l’interlocuteur de ladite composante avant d’être soumis à facturation. 

Par ailleurs, chaque BDL est adapté au client (voire à la composante), en effet, l’unité d’ouvrage peut être la demi-journée, la journée ou l’heure. Parfois on peut même avoir des heures d’arrivées et de départ pour chaque demi journée.

C’est donc dans l’optique de moderniser la gestion des bons de livraisons que Perform Vision souhaite mettre en place un site extranet dynamique. 

## Uses cases

Ce site devra permettre à chaque utilisateur (suivant son rôle) :

● aux prestataire de : 

    ○ se connecter (et changer ses mots de passes) 

    ○ renseigner pour chacun de ses clients le bon de livraisons par le biais du formulaire adéquate : 

        ■ au fur et à mesure au cours du mois, 

        ■ ou en une seule fois lorsque le mois est échu 

    ○ de déclarer en avance des absences (optionnel) 

    ○ de générer un pdf de sa déclaration et le signer avec une signature électronique 

    ○ de générer un csv de sa/ses déclarations (optionnels) 

● aux interlocuteurs de composantes client de : 

    ○ se connecter (et changer ses mots de passes) 

    ○ consulter la liste des prestataires (et leur coordonnées) qui sont affectées à sa composante 

    ○ consulter les BDL de chaque prestataire et : 

        ■ générer un pdf du BDL et le signer avec une signature électronique 

        ■ cocher les journées qui lui posent problème en cas de désaccord 

        ■ ajout de commentaires 

    ○ consulter les déclarations d’absences (optionnels) 

● au commercial : 

    ○ afficher la liste de ses clients/composantes/interlocuteurs/prestataires 

    ○ ajouter un interlocuteur client (ou modifier certains champs) 

    ○ consulter les BDL des prestataires affectés à ses interlocuteurs clients 

● au gestionnaire : 

    ○ ajouter des prestataires (ou modifier certains champs) 

    ○ gérer les affectations des prestataires dans les composantes 

    ○ ajouter/modifier un client et lui ajouter au moins une composante, et affecter cette dernière à au moins un commercial 

    ○ consulter les BDLs des prestataires (et les déclarations d’absences) 

● à l’administrateur (dispose également des rôle gestionnaire et commercial) :

    ○ ajouter des gestionnaires

## Déploiement

### Prérequis

- Serveur Web (Apache, Nginx, ...)
- PHP installé (Avec l'extension pgsql)
- PostgreSQL installé

### Installation

Vous trouverez le site dans le dossier Site/MVC.

- Téléchargez le code source du site depuis le dépôt.
- Lancez votre Serveur Web.
- Lancez PostgreSQL
- Modifiez les informations de connexion à la base de données dans /Site/MVC/Utils/credentials.php
- Créez les tables de la base de données avec le script SQL dans le dossier /EC1/RSA/adaptation.sql
- Insérez les données dans la base de données avec le script dans le dossier /EC1/RSA/insert.sql
- Rendez vous à l'url localhost ou à l'adresse 127.0.0.1






